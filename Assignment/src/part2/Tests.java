package part2;

public class Tests {

	public static void main(String[] args) 
	{
		BinarySearchTree<Double> tree = new BinarySearchTree<Double>(true);
		
		tree.remove(10.0);
		boolean contains = tree.contains(5.0);
		assert !contains : "Contains method not returning as expected.";
		String treeOutput = tree.toString();
		int height = tree.height();
		assert treeOutput == "": "Output of BST not as expected.";
		assert height == -1: "Height of BST not as expected.";
		System.out.println("Tree: \n" + treeOutput + "\nHeight: " + height + "\n");
		
		tree.add(5.0);
		contains = tree.contains(5.0);
		assert contains : "Contains method not returning as expected.";
		treeOutput = tree.toString();
		height = tree.height();
		assert treeOutput == "5.0; ": "Output of BST not as expected.";
		assert height == 0: "Height of BST not as expected.";
		System.out.println("Tree: \n" + treeOutput + "\nHeight: " + height + "\n");
		
		tree.add(4.0);
		tree.add(5.0);
		contains = tree.contains(4.2);
		assert contains : "Contains method not returning as expected.";
		treeOutput = tree.toString();
		height = tree.height();
		assert treeOutput == "5.0; 4.2; 5.1; ": "Output of BST not as expected.";
		assert height == 2: "Height of BST not as expected.";
		System.out.println("Tree: \n" + treeOutput + "\nHeight: " + height + "\n");
		
		tree.add(3.0);
		tree.add(5.2);
		tree.add(4.5);
		tree.add(5.05);
		contains = tree.contains(15.0);
		assert !contains : "Contains method not returning as expected.";
		treeOutput = tree.toString();
		height = tree.height();
		assert treeOutput == "5.0; 4.2; 5.1; 3.0; 4.5; 5.05; 5.2": "Output of BST not as expected.";
		assert height == 6: "Height of BST not as expected.";
		System.out.println("Tree: \n" + treeOutput + "\nHeight: " + height + "\n");

		tree.remove(3.0);
		treeOutput = tree.toString();
		height = tree.height();
		assert treeOutput == "5.0; 4.2; 5.1; 4.5; 5.05; 5.2": "Output of BST not as expected.";
		assert height == 6: "Height of BST not as expected.";
		System.out.println("Tree: \n" + treeOutput + "\nHeight: " + height + "\n");
	}

}
