package part3;
import java.util.AbstractSet;
import java.util.Random;
import java.util.TreeSet;

public class Tests {

	public static void main(String[] args) 
	{
		Random randomGen = new Random();

		System.out.println("Pure Random additions");
		//First for loop is for the number of items to add to the tree, in this case I have chose 2^i items in each tree at each iteration. 
		for(int power=0; power<21; power++)
		{
			BinarySearchTree<Item> leafInsertTree = new BinarySearchTree<Item>();
			BinarySearchTree<Item> rootInsertTree = new BinarySearchTree<Item>(true);
			TreeSet<Item> treeSet = new TreeSet<Item>();
			
			Item.resetCompCount();
			int numIterations = (int) Math.pow(2, power);

			System.out.println("Pure Random additions");
			System.out.println(numIterations + " elements :");
			 
			int successfulAdds = addToTree(randomGen, leafInsertTree, numIterations);
			System.out.println("Leaf Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.");
			Item.resetCompCount();

			successfulAdds = addToTree(randomGen, rootInsertTree, numIterations);
			System.out.println("Root Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.");
			Item.resetCompCount();

			successfulAdds = addToTree(randomGen, treeSet, numIterations);
			System.out.println("TreeSet Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.\n");
		}
		
		System.out.println("Finished!");

		//A new loop doing the same as the previous, but will have far more duplicate numbers generated. 
		System.out.println("\nLimited range random additions");
		for(int power=0; power<21; power++)
		{
			BinarySearchTree<Item> leafInsertTree = new BinarySearchTree<Item>();
			BinarySearchTree<Item> rootInsertTree = new BinarySearchTree<Item>(true);
			TreeSet<Item> treeSet = new TreeSet<Item>();
			
			Item.resetCompCount();
			int numIterations = (int) Math.pow(2, power);

			System.out.println(numIterations + " elements :");
			 
			int successfulAdds = addToTree(randomGen, leafInsertTree, numIterations, numIterations/3 + 1);
			System.out.println("Leaf Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.");
			Item.resetCompCount();

			successfulAdds = addToTree(randomGen, rootInsertTree, numIterations, numIterations/3 + 1);
			System.out.println("Root Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.");
			Item.resetCompCount();

			successfulAdds = addToTree(randomGen, treeSet, numIterations, numIterations/3 + 1);
			System.out.println("TreeSet Insertion : " + Item.getCompCount() + " comparisons. " + successfulAdds + 
					" successful adds and " + (numIterations - successfulAdds) + " unsuccessful adds.\n");
		}
		
		System.out.println("Finished!");
	}
	
	private static int addToTree(Random generator, AbstractSet<Item> tree, int numItems)
	{
		int successfulAdds = 0;
		for(int i=0; i<numItems; i++)
		{
			if(tree.add(new Item(generator.nextInt())))
				successfulAdds++;
		}
		return successfulAdds;
	}
	private static int addToTree(Random generator, AbstractSet<Item> tree, int numItems, int bound)
	{
		int successfulAdds = 0;
		for(int i=0; i<numItems; i++)
		{
			if(tree.add(new Item(generator.nextInt(bound))))
				successfulAdds++;
		}
		return successfulAdds;
	}

}
